<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle\Contracts;

interface PaginateManagerInterface
{
    public function setQuery($builder);

    public function skip(int $offset);

    public function take(int $perPage);

    public function get();

    public function count();
}
