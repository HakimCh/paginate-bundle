<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle\Exceptions;

class BuilderNotFoundException extends \Exception
{
}
