<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle\Manager;

use Doctrine\ORM\QueryBuilder;
use HakimCh\PaginateBundle\Contracts\PaginateManagerInterface;
use HakimCh\PaginateBundle\Exceptions\BuilderNotFoundException;

class DoctrineManager implements PaginateManagerInterface
{
    /**
     * @var QueryBuilder
     */
    private $builder;

    public function setQuery($builder): self
    {
        if (!$builder instanceof QueryBuilder) {
            throw new BuilderNotFoundException(sprintf(
                'Fatal Error: %s() excepts parameter 1 to be %s, %s given',
                __METHOD__,
                QueryBuilder::class,
                \gettype($builder)
            ));
        }
        $this->builder = $builder;

        return $this;
    }

    public function skip(int $offset): self
    {
        $this->builder->setFirstResult($offset);

        return $this;
    }

    public function take(int $perPage): self
    {
        $this->builder->setMaxResults($perPage);

        return $this;
    }

    public function get()
    {
        return $this->builder->getQuery()->getResult();
    }

    public function count()
    {
        $builder = clone $this->builder;
        $aliases = $builder->getRootAliases();

        return $builder->select('COUNT('.$aliases[0].'.id) as nbr')->getQuery()->getSingleScalarResult();
    }
}
