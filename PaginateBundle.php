<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PaginateBundle extends Bundle
{
}
