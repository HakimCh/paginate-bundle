<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle\Services;

use HakimCh\PaginateBundle\Contracts\PaginateManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Paginate
{
    public const SEPARATOR = '...';
    public const PAGE_NAME = 'page';
    public const VISIBLE_LINKS = 3;

    private $itemsPerPage = 15;
    private $currentPage = 1;
    /**
     * @var PaginateManagerInterface
     */
    private $manager;
    /**
     * @var PaginateUrl
     */
    private $url;

    public function __construct(PaginateManagerInterface $manager, RequestStack $requestStack)
    {
        $this->manager = $manager;
        $this->paginateUrl = new PaginateUrl($requestStack->getCurrentRequest());
        $this->currentPage = $this->paginateUrl->getCurrentPage(self::PAGE_NAME);
    }

    /**
     * @param int $itemsPerPage
     *
     * @return Paginate
     */
    public function setItemsPerPage(int $itemsPerPage): self
    {
        $this->itemsPerPage = $itemsPerPage;

        return $this;
    }

    /**
     * @param object $query
     *
     * @return array
     */
    public function make($query): array
    {
        $this->manager->setQuery($query);
        $pagesNumber = (int) ceil($this->manager->count() / $this->itemsPerPage);
        $offset = $this->itemsPerPage * ($this->currentPage - 1);
        $pages = $this->getPagesDefinitions($pagesNumber);

        return [
            'links' => $this->getLinks($pages),
            'items' => $this->manager->skip($offset)
                ->take($this->itemsPerPage)
                ->get(),
        ];
    }

    private function getLinks(array $pages): array
    {
        $links = [];
        foreach ($pages as $page) {
            $links[] = $this->addLink($page);
        }

        return $links;
    }

    /**
     * @param int $pagesNumber
     *
     * @return array
     */
    private function getPagesDefinitions(int $pagesNumber): array
    {
        $visibleItems = self::VISIBLE_LINKS * 3;
        if ($pagesNumber <= $visibleItems) {
            return range(1, $pagesNumber);
        }
        $thirdGroupOffset = $pagesNumber - self::VISIBLE_LINKS + 1;
        $firstGroup = range(1, self::VISIBLE_LINKS);
        $thirdGroup = range($thirdGroupOffset, $pagesNumber);
        if ($this->currentPage <= self::VISIBLE_LINKS || $this->currentPage >= $thirdGroupOffset) {
            return array_merge($firstGroup, [null], $thirdGroup);
        }
        $secondGroup = [$this->currentPage - 1, $this->currentPage, $this->currentPage + 1];

        return array_merge($firstGroup, [null], $secondGroup, [null], $thirdGroup);
    }

    /**
     * @param int $page
     *
     * @return array
     */
    private function addLink(?int $page = null): array
    {
        if ($page) {
            return [
                'text' => $page,
                'href' => $this->paginateUrl->getPaginateUrl($page),
                'active' => $this->currentPage === $page,
            ];
        }

        return [
            'href' => '#',
            'text' => self::SEPARATOR,
            'disabled' => true,
        ];
    }
}
