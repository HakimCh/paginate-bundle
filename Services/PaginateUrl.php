<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle\Services;

use Symfony\Component\HttpFoundation\Request;

class PaginateUrl
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the full current url without the pagination suffix.
     *
     * @return string
     */
    public function getPaginateBaseUrl(): string
    {
        $currentUrl = $this->request->getSchemeAndHttpHost().$this->request->getPathInfo();
        if ($baseUrl = strstr($currentUrl, '/page/', true)) {
            return $baseUrl;
        }

        return $currentUrl;
    }

    /**
     * Get the pagination url by the page number.
     *
     * @param int $page
     *
     * @return string
     */
    public function getPaginateUrl(int $page = 1): string
    {
        return sprintf('%s/page/%d', $this->getPaginateBaseUrl(), $page);
    }

    /**
     * Get the pagination current page.
     *
     * @param string $p$flagage
     * @param string $flag
     *
     * @return int
     */
    public function getCurrentPage(string $flag): int
    {
        return $this->request->attributes->has($flag) ? $this->request->attributes->get($flag) : 1;
    }
}
