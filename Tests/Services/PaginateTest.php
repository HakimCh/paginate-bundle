<?php

declare(strict_types=1);

namespace HakimCh\PaginateBundle\Tests\Services;

use HakimCh\Helpers\Http\Request;
use HakimCh\PaginateBundle\Manager\DoctrineManager;
use HakimCh\PaginateBundle\Services\Paginate;
use PHPUnit\Framework\TestCase;

class PaginateTest extends TestCase
{
    public function setUp()
    {
        $this->paginate = $this->setUpPaginate(1);
    }

    public function testMake()
    {
        $result = $this->paginate->make(new \stdClass());
        $this->assertArrayHasKey('items', $result);
    }

    public function testGetLinks()
    {
        $result = $this->callPrivateMethod('getLinks', [1, null, 5]);
        $expected = [
          ['href' => 'http://fake-host/fake-uri/page/1', 'text' => 1, 'active' => true],
          ['href' => '#', 'text' => Paginate::SEPARATOR, 'disabled' => true],
          ['href' => 'http://fake-host/fake-uri/page/5', 'text' => 5, 'active' => false],
        ];
        $this->assertEquals($expected, $result);
    }

    public function testGetPagesDefinitions()
    {
        $this->paginate = $this->setUpPaginate(6);
        $result = $this->callPrivateMethod('getPagesDefinitions', 20);
        $this->assertEquals([1, 2, 3, null, 5, 6, 7, null, 18, 19, 20], $result);
    }

    public function testAddLinkWithInteger()
    {
        $link = $this->callPrivateMethod('addLink', 2);
        $this->assertEquals(['href' => 'http://fake-host/fake-uri/page/2', 'text' => 2, 'active' => false], $link);
    }

    public function testAddLinkWithIntegerEqualTheCurrentPage()
    {
        $link = $this->callPrivateMethod('addLink', 1);
        $this->assertEquals(['href' => 'http://fake-host/fake-uri/page/1', 'text' => 1, 'active' => true], $link);
    }

    public function testAddLinkWithNull()
    {
        $link = $this->callPrivateMethod('addLink', null);
        $this->assertEquals(['href' => '#', 'text' => Paginate::SEPARATOR, 'disabled' => true], $link);
    }

    private function callPrivateMethod($name, $argument)
    {
        $class = new \ReflectionClass($this->paginate);
        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return $method->invokeArgs($this->paginate, [$argument]);
    }

    private function setUpPaginate(int $currentPage): Paginate
    {
        $items = range(1, 100);
        $count = \count($items);
        $request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCurrentPage', 'getPaginateUrl'])
            ->getMock();

        $request->expects($this->any())
            ->method('getCurrentPage')
            ->willReturn($currentPage);

        $paginateUrlsMap = [];
        for ($page = 1; $page <= 20; ++$page) {
            $paginateUrlsMap[] = [$page, sprintf('http://fake-host/fake-uri/page/%s', $page)];
        }

        $request->expects($this->any())
            ->method('getPaginateUrl')
            ->willReturnMap($paginateUrlsMap);

        $manager = $this->getMockBuilder(DoctrineManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['count', 'skip', 'take', 'get'])
            ->getMock();

        $manager->expects($this->any())
            ->method('get')
            ->willReturn($items);

        $manager->expects($this->any())
            ->method('count')
            ->willReturn($count);

        $paginate = new Paginate($manager, $request);
        $paginate->setItemsPerPage(10);

        return $paginate;
    }
}
